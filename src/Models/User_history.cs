﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SproApp.Models
{
    public class User_history
    {
        [Key]
        public Int32 id_us { get; set; }
        public String titulo { get; set; }
        public String descripcion { get; set; }
        public Int32 tiempo_estimado { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public Nullable<int> id_proyecto { get; set; }
        public virtual Proyecto Proyecto { get; set; }
        public Nullable<int> id_sb { get; set; }
        public virtual Sprint_backlog Sprint_backlog { get; set; }
    }
}
