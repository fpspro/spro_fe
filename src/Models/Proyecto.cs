﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SproApp.Models
{
    public class Proyecto
    {
        public Proyecto()
        {
            this.User_historys = new HashSet<User_history>();
        }
        [Key]
        public Int32 id_proyecto { get; set; }
        public String nombre { get; set; }
        public Int32 anho { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public String descripcion { get; set; }
        public virtual ICollection<User_history> User_historys { get; set; }
    }
}
