﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SproApp.Models
{
    public class Estado
    {
        public Estado()
        {
            this.Sprint_backlogs = new HashSet<Sprint_backlog>();
        }
        [Key]
        public Int32 Id_es { get; set; }
        public String Titulo_estado { get; set; }
        public DateTime Creado { get; set; }
        public DateTime Modificado { get; set; }
        public virtual ICollection<Sprint_backlog> Sprint_backlogs { get; set; }
    }
}
