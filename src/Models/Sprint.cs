﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SproApp.Models
{
    public class Sprint
    {
        public Sprint()
        {
            this.Sprint_backlogs = new HashSet<Sprint_backlog>();
        }
        [Key]
        public Int32 id_spr { get; set; }
        public String titulo { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public String descripcion { get; set; }
        public virtual ICollection<Sprint_backlog> Sprint_backlogs { get; set; }
    }
}
