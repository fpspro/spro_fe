﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Company.WebApplication1.Migrations
{
    public partial class migration03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "id_sb",
                table: "User_history",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Estado",
                columns: table => new
                {
                    Id_es = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Titulo_estado = table.Column<string>(nullable: true),
                    Creado = table.Column<DateTime>(nullable: false),
                    Modificado = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estado", x => x.Id_es);
                });

            migrationBuilder.CreateTable(
                name: "Sprint",
                columns: table => new
                {
                    id_spr = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    titulo = table.Column<string>(nullable: true),
                    fecha_inicio = table.Column<DateTime>(nullable: false),
                    fecha_fin = table.Column<DateTime>(nullable: false),
                    creado = table.Column<DateTime>(nullable: false),
                    modificado = table.Column<DateTime>(nullable: false),
                    descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sprint", x => x.id_spr);
                });

            migrationBuilder.CreateTable(
                name: "Sprint_backlog",
                columns: table => new
                {
                    id_sb = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    complejidad = table.Column<int>(nullable: false),
                    fecha_inicio = table.Column<DateTime>(nullable: false),
                    fecha_fin = table.Column<DateTime>(nullable: false),
                    creado = table.Column<DateTime>(nullable: false),
                    modificado = table.Column<DateTime>(nullable: false),
                    id_tarea = table.Column<int>(nullable: true),
                    id_sprint = table.Column<int>(nullable: true),
                    id_estado = table.Column<int>(nullable: true),
                    id_asignado = table.Column<string>(nullable: true),
                    SprintId_spr = table.Column<int>(nullable: true),
                    EstadoId_es = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sprint_backlog", x => x.id_sb);
                    table.ForeignKey(
                        name: "FK_Sprint_backlog_Estado_EstadoId_es",
                        column: x => x.EstadoId_es,
                        principalTable: "Estado",
                        principalColumn: "Id_es",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sprint_backlog_Sprint_SprintId_spr",
                        column: x => x.SprintId_spr,
                        principalTable: "Sprint",
                        principalColumn: "Id_spr",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_history_id_sb",
                table: "User_history",
                column: "id_sb",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sprint_backlog_EstadoId_es",
                table: "Sprint_backlog",
                column: "EstadoId_es");

            migrationBuilder.CreateIndex(
                name: "IX_Sprint_backlog_SprintId_spr",
                table: "Sprint_backlog",
                column: "SprintId_spr");

            migrationBuilder.AddForeignKey(
                name: "FK_User_history_Sprint_backlog_id_sb",
                table: "User_history",
                column: "id_sb",
                principalTable: "Sprint_backlog",
                principalColumn: "id_sb",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_history_Sprint_backlog_id_sb",
                table: "User_history");

            migrationBuilder.DropTable(
                name: "Sprint_backlog");

            migrationBuilder.DropTable(
                name: "Estado");

            migrationBuilder.DropTable(
                name: "Sprint");

            migrationBuilder.DropIndex(
                name: "IX_User_history_id_sb",
                table: "User_history");

            migrationBuilder.DropColumn(
                name: "id_sb",
                table: "User_history");
        }
    }
}
