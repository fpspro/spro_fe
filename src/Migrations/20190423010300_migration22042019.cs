﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Company.WebApplication1.Migrations
{
    public partial class migration22042019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Nombre",
                table: "Proyecto",
                newName: "nombre");

            migrationBuilder.RenameColumn(
                name: "Descripcion",
                table: "Proyecto",
                newName: "descripcion");

            migrationBuilder.RenameColumn(
                name: "Anho",
                table: "Proyecto",
                newName: "anho");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Proyecto",
                newName: "id_proyecto");

            migrationBuilder.CreateTable(
                name: "User_history",
                columns: table => new
                {
                    id_us = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    titulo = table.Column<string>(nullable: true),
                    descripcion = table.Column<string>(nullable: true),
                    tiempo_estimado = table.Column<int>(nullable: false),
                    creado = table.Column<DateTime>(nullable: false),
                    modificado = table.Column<DateTime>(nullable: false),
                    id_proyecto = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_history", x => x.id_us);
                    table.ForeignKey(
                        name: "FK_User_history_Proyecto_id_proyecto",
                        column: x => x.id_proyecto,
                        principalTable: "Proyecto",
                        principalColumn: "id_proyecto",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_history_id_proyecto",
                table: "User_history",
                column: "id_proyecto");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User_history");

            migrationBuilder.RenameColumn(
                name: "nombre",
                table: "Proyecto",
                newName: "Nombre");

            migrationBuilder.RenameColumn(
                name: "descripcion",
                table: "Proyecto",
                newName: "Descripcion");

            migrationBuilder.RenameColumn(
                name: "anho",
                table: "Proyecto",
                newName: "Anho");

            migrationBuilder.RenameColumn(
                name: "id_proyecto",
                table: "Proyecto",
                newName: "Id");
        }
    }
}
