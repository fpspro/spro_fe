﻿// Write your Javascript code.
var url = '';
var d;
var values;
var $grilla;
; (function ($) {

    'use strict';

    $('.alert[data-auto-dismiss]').each(function (index, element) {
        var $element = $(element),
            timeout = $element.data('auto-dismiss') || 5000;

        setTimeout(function () {
            //$element.alert('close');
            $element.fadeTo(1000, 500).slideUp(500, function () {
                $element.alert('close');
            });
        }, timeout);
    });

})(jQuery);

$('#btn-user').click(function () {
    $('#ModalUser').hide();
});


$("#ver_proyectos").click(function () {
    $("#contenedor-principal").load("secciones/grilla.html", function () {
        console.log('carga proyectos');
        $(".box-title").html('PROYECTOS');
        grilla_procesos.init('#grilla-general', 'proyectos', 0);
    });

});
$("#ver_sprints").click(function () {
    $("#contenedor-principal").load("secciones/grilla.html", function () {
        $(".box-title").html('SPRINTS');
        grilla_procesos.init('#grilla-general', 'sprints', 0);
    });

});

$("#ver_usuarios").click(function () {
    $("#contenedor-principal").load("secciones/grilla.html", function () {
        console.log('carga usuarios');
        $(".box-title").html('USUARIOS');
        grilla_procesos.init('#grilla-general','usuarios', 0);
    });

});

var grilla_procesos = {
    _id: '',
    _operacion: '',
    _idparam: 0,
    init: function (_idgrid, _operacion, _idparam) {  
        this._id = _idgrid;
        this._operacion = _operacion;
        this._idparam = _idparam;
        //console.log(_id + ' .box-body');
        if ($(this._id + ' .box-body').html().length > 0) {
            $(this._id + ' .box-body').jsGrid('destroy');
        }

        this.ajustar_panel();
        this.construir_grilla();
        this.cargar_comandos();
    },

    ajustar_panel: function () {
        var _id = this._id;
        var altura = $(window).height() - $('#contenedor-principal').position().top - $('.main-footer').outerHeight() - 10;
        $('#contenedor-principal')
            .css({ 'overflow-y': 'auto' })
            .height(altura);

        $(_id).height(altura - 10);
        $(_id + ' .box-body').height($(_id).height() - $(_id + ' .box-header').outerHeight());
    },

    construir_grilla: function () {
        var siteurl = '';
        var campos = [];
        var imagesSource = (location.href.indexOf('public') > -1) ? '../public/img/' : '../img/';
        var that = this;
        var _id = this._id;
        //Segun la _operacion pasada como parametro se asigna las columnas de la grilla
        if (this._operacion === 'usuarios') {
             campos = [
                {
                    type: "control", width: 35, deleteButton: false, editButton: false, modeSwitchButton: false,

                    headerTemplate: function () {
                        return $("<button>")
                            .attr("type", "button")
                            .attr("title", "agregar nuevo [Crtl+i]")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .addClass('btn_add_new')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_add_circle_black_18dp_2x.png" style="height:30px;">')
                            .click(function () {
                                that.ver_elemento();
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });
                    },

                    itemTemplate: function (value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customButton = $("<button>")
                            .attr("title", "ver detalles o realizar acciones")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_more_horiz_black_36dp_1x.png" style="height:30px;">')
                            .click(function (e) {
                                e.preventDefault();
                                that.ver_elemento(item);
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });

                        return $result.add($customButton);
                    }
                },
                { name: "Id", title: "Id", type: "number", width: 65, align: "right" },
                { name: "UserName", title: "Nombre", type: "text", width: 60 },
                { name: "Email", title: "Email", type: "text", width: 60 },

            ];
             // se define la direccion de la API para obtener los datos respectivos
            siteurl = 'https://localhost:44381/api/users';
        }
        if (this._operacion === 'proyectos') {
            campos = [
                {
                    type: "control", width: 35, deleteButton: false, editButton: false, modeSwitchButton: false,

                    headerTemplate: function () {
                        return $("<button>")
                            .attr("type", "button")
                            .attr("title", "agregar nuevo [Crtl+i]")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .addClass('btn_add_new')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_add_circle_black_18dp_2x.png" style="height:30px;">')
                            .click(function () {
                                that.ver_elemento();
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });
                    },

                    itemTemplate: function (value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customButton = $("<button>")
                            .attr("title", "ver detalles o realizar acciones")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_more_horiz_black_36dp_1x.png" style="height:30px;">')
                            .click(function (e) {
                                e.preventDefault();
                                that.ver_elemento(item);
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });

                        return $result.add($customButton);
                    }
                },
                { name: "id_proyecto", title: "Id", type: "number", width: 30, align: "right" },
                { name: "nombre", title: "Proyecto", type: "text", width: 75 },
                { name: "fecha_inicio", title: "Fecha inicio", type: "date", width: 50, align: "center"  },
                { name: "fecha_fin", title: "Fecha Fin", type: "date", width: 50, align: "center"  }

            ];
            // se define la direccion de la API para obtener los datos respectivos
            siteurl = 'https://localhost:44381/api/proyectos';
        }
        if (this._operacion === 'sprints') {
            campos = [
                {
                    type: "control", width: 35, deleteButton: false, editButton: false, modeSwitchButton: false,

                    headerTemplate: function () {
                        return $("<button>")
                            .attr("type", "button")
                            .attr("title", "agregar nuevo [Crtl+i]")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .addClass('btn_add_new')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_add_circle_black_18dp_2x.png" style="height:30px;">')
                            .click(function () {
                                that.ver_elemento();
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });
                    },

                    itemTemplate: function (value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customButton = $("<button>")
                            .attr("title", "ver detalles o realizar acciones")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_more_horiz_black_36dp_1x.png" style="height:30px;">')
                            .click(function (e) {
                                e.preventDefault();
                                that.ver_elemento(item);
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });

                        return $result.add($customButton);
                    }
                },
                { name: "id_spr", title: "Id", type: "number", width: 30, align: "right" },
                { name: "titulo", title: "Título", type: "text", width: 75 },
                { name: "fecha_inicio", title: "Fecha inicio", type: "date", width: 50, align: "center" },
                { name: "fecha_fin", title: "Fecha Fin", type: "date", width: 50, align: "center" }

            ];
            // se define la direccion de la API para obtener los datos respectivos
            siteurl = 'https://localhost:44381/api/sprint';
        }
        if (this._operacion === 'user_history') {
            campos = [
                {
                    type: "control", width: 35, deleteButton: false, editButton: false, modeSwitchButton: false,

                    headerTemplate: function () {
                        return $("<button>")
                            .attr("type", "button")
                            .attr("title", "agregar nuevo [Crtl+i]")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .addClass('btn_add_new')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_add_circle_black_18dp_2x.png" style="height:30px;">')
                            .click(function () {
                                that.ver_elemento();
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });
                    },

                    itemTemplate: function (value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customButton = $("<button>")
                            .attr("title", "ver detalles o realizar acciones")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_more_horiz_black_36dp_1x.png" style="height:30px;">')
                            .click(function (e) {
                                e.preventDefault();
                                that.ver_elemento(item);
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });

                        return $result.add($customButton);
                    }
                },
                { name: "id_us", title: "Id", type: "number", width: 30, align: "right" },
                { name: "titulo", title: "Titulo", type: "text", width: 75 },
                { name: "tiempo_estimado", title: "Tiempo Estimado", type: "number", width: 50, align: "center" },
                { name: "creado", title: "Creación", type: "date", width: 50, align: "center" }

            ];
            // se define la direccion de la API para obtener los datos respectivos
            if (this._idparam > 0)
                siteurl = 'https://localhost:44381/api/userhistorys/' + this._idparam;
            else
                siteurl = 'https://localhost:44381/api/userhistorys';
        }
        if (this._operacion === 'sprint_backlog') {
            campos = [
                {
                    type: "control", width: 35, deleteButton: false, editButton: false, modeSwitchButton: false,

                    headerTemplate: function () {
                        return $("<button>")
                            .attr("type", "button")
                            .attr("title", "agregar nuevo [Crtl+i]")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .addClass('btn_add_new')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_add_circle_black_18dp_2x.png" style="height:30px;">')
                            .click(function () {
                                that.ver_elemento();
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });
                    },

                    itemTemplate: function (value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customButton = $("<button>")
                            .attr("title", "ver detalles o realizar acciones")
                            .addClass('btn')
                            .addClass('btn-default')
                            .addClass('btn-xs')
                            .css({ 'padding': '0px', 'min-width': '25px', 'border': 'none' })
                            .html('<img src="' + imagesSource + 'ic_more_horiz_black_36dp_1x.png" style="height:30px;">')
                            .click(function (e) {
                                e.preventDefault();
                                that.ver_elemento(item);
                            }).focus(function () {
                                $(this).css({ 'border': '1px solid #2a8dd4' });
                            }).focusout(function () {
                                $(this).css({ 'border': 'none' });
                            });

                        return $result.add($customButton);
                    }
                },
                { name: "id_sb", title: "Id", type: "number", width: 30, align: "right" },
                { name: "id_asignado", title: "Asginado", type: "text", width: 75 },
                { name: "complejidad", title: "Complejidad", type: "number", width: 50, align: "center" },
                { name: "fecha_inicio", title: "Fecha Inicio", type: "date", width: 50, align: "center" },
                { name: "fecha_fin", title: "Fecha Fin", type: "date", width: 50, align: "center" }

            ];
            // se define la direccion de la API para obtener los datos respectivos
            if (this._idparam > 0)
                siteurl = 'https://localhost:44381/api/sprintbacklogs/' + this._idparam;
            else
                siteurl = 'https://localhost:44381/api/sprintbacklogs';
        }

        $grilla = {
            height: "100%",
            width: "100%",
            autoload: true,
            filtering: true,
            sorting: true,
            pageSize: 10,
            pageButtonCount: 5,
            noDataContent: "Sin Informacion Encontrada",
            loadMessage: "Por Favor Aguarde...",
            pagePrevText: 'Prev',
            pageNextText: 'Next',
            pageFirstText: "Primero",
            pageLastText: "Ultimo",
            pageTittleText: "Paginas",
            rowDoubleClick: function (args) {
                //that.ver_usuario(args.item)
            },
            onOptionChanging: function (args) {
                if (args.option === "data") {
                    $.each(args.newValue, function (index, value) {
                        //this.creacion = formatear_fecha_hora(this.creacion)
                    });
                }
            },

            onDataLoaded: function (args) {

            },

            controller: {
                contarTotales: function (conf) {
                    //console.log(conf)
                    conf.contar = true;

                    var $opciones = {
                        url: siteurl,
                        type: "GET",
                        data: { conf: JSON.stringify(conf) },
                        error: function (error) { console.log("Error: " + JSON.stringify(error)); console.log(this.url); },
                        success: function (data) {
                            //console.log(data)
                        }
                    };
                    return $.ajax($opciones);
                },
                loadData: function (conf) {
                    //console.log(conf)
                    conf.contar = false;

                    d = $.Deferred();

                    var $opciones = {
                        url: siteurl,
                        type: "GET",
                        dataType: "json",
                        data: { conf: JSON.stringify(conf) },
                        error: function (error) { console.log("Error: " + JSON.stringify(error)); console.log(this.url); },
                        success: function (data) {
                            //console.log(data)
                            d.resolve(data);
                        }
                    };
                    $.ajax($opciones);
                    return d.promise();
                }
            },
            fields: campos
        };

        $(_id + ' .box-body').jsGrid($grilla);

    },

    cargar_comandos: function () {
        var that = this;
        var _id = this._id;

        $(window).resize(function () {
            that.ajustar_panel();
            $(_id + ' .box-body').jsGrid('refresh');
            $( _id + ' .box-body a.search_button').trigger('click');
        });

        $(window).off('keydown');
        $(window).on('keydown', function (event) {
            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 'f':
                        event.preventDefault();
                        $(_id + ' .box-body a.search_button').trigger('click');
                        break;
                }
            }

            if (event.altKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 'i':
                        event.preventDefault();
                        $(_id + ' button.btn_add_new').trigger('click');
                        break;
                }
            }
        });
    },

    ver_elemento: function (item) {
        values = item;
       
        if (item === undefined) {
            //Modal de creacion
            switch (this._operacion ) {
                case 'usuarios':
                    $(".modal-title").html('Nuevo Usuario');
                    $('#ModalUser').modal();
                    break;
                case 'proyectos':
                    $("#Modalform .modal-body").load("secciones/proyectoform.html", function () {
                        //console.log(values.id);
                        $("#pro-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-createpro" class="btn pull-right">Guardar</button> </div>');
                        $(".modal-body").css("height", "430px");
                        $(".modal-title").html('Nuevo Proyecto');
                        $('#Modalform').modal();
                        $('#btn-createpro').click(function () {
                            $('.close').trigger("click");
                            var params = new Object();
                            params.id = $("#id_pro").val();
                            params.nombre = $("#name_pro").val();
                            params.fechainicio = $("#fecha_inicio").val();
                            params.fechafin = $("#fecha_fin").val();
                            params.descripcion = $("#descrip_pro").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/proyectos",
                                type: "POST",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    $('#ver_proyectos').trigger("click");
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                        });
                    });
                    break;
                case 'sprints':
                    $("#Modalform .modal-body").load("secciones/sprintform.html", function () {
                        //console.log(values.id);
                        $("#sprint-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-createspr" class="btn pull-right">Guardar</button> </div>');
                        $(".modal-body").css("height", "430px");
                        $(".modal-title").html('Nuevo Sprint');
                        $('#Modalform').modal();
                        $('#btn-createspr').click(function () {
                            $('.close').trigger("click");
                            var params = new Object();   
                            params.titulo = $("#name_sprint").val();
                            params.fechainicio = $("#fecha_inicio_spr").val();
                            params.fechafin = $("#fecha_fin_spr").val();
                            params.descripcion = $("#descrip_spr").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/sprint",
                                type: "POST",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    $('#ver_sprints').trigger("click");
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                        });
                    });
                    break;
                case 'user_history':
                    $("#Modalform .modal-body").load("secciones/userhistoryform.html", function () {
                        //console.log(values.id);
                        $("#us-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-createus" class="btn pull-right">Guardar</button> </div>');
                        $(".modal-body").css("height", "430px");
                        $(".modal-title").html('Nueva Historia de Usuario');
                        $('#Modalform').modal();
                        $('#btn-createus').click(function () {
                            $('.close').trigger("click");
                            var params = new Object();
                            params.id = $("#id_us").val();
                            params.titulo = $("#titulo_us").val();
                            params.tiempo_estimado = $("#tiempo_estimado").val();
                            params.descripcion = $("#descrip_us").val();
                            params.id_proyecto = $("#id_pro").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/userhistorys",
                                type: "POST",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    grilla_procesos.init('#grilla-user-history', 'user_history', params.id_proyecto);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                        });
                        $('#Modalform').modal();
                    });
                    break;
                case 'sprint_backlog':
                    $("#Modalform .modal-body").load("secciones/sprintbacklogform.html", function () {
                        //console.log(values.id);
                        $("#sb-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-create-sb" class="btn pull-right">Guardar</button> </div>');
                        $(".modal-body").css("height", "600px");
                        $(".modal-title").html('Nuevo Sprint Backlog');
                        $('#Modalform').modal();
                        $('#btn-create-sb').click(function () {
                            $('.close').trigger("click");
                            var params = new Object();
                            params.id = $("#id_us").val();
                            params.titulo = $("#titulo_us").val();
                            params.tiempo_estimado = $("#tiempo_estimado").val();
                            params.descripcion = $("#descrip_us").val();
                            params.id_proyecto = $("#id_pro").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/userhistorys",
                                type: "POST",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    grilla_procesos.init('#grilla-user-history', 'user_history', params.id_proyecto);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                        });
                        $('#Modalform').modal();
                    });
                    break;
            }


        } else {
           
          //Modal de modificacion o Formulario de Proyecto
            switch (this._operacion) {
                case 'usuarios':
                    $(".modal-body").css("height", "265px");
                    $(".modal-title").html('Modificar Usuario');
                    $("#Modalform .modal-body").load("secciones/usuarioform.html", function () {
                        //console.log(values.id);
                        $("#id_user").val(values.Id);
                        $("#name_user").val(values.UserName);
                        $("#email_user").val(values.Email);
                        $('#btn-update').click(function () {
                            var params = {};
                            params.id = $("#id_user").val();
                            params.name = $("#name_user").val();
                            params.email = $("#email_user").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/users",
                                type: "POST",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);

                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                            $('.close').trigger("click");
                        });
                    });
                    $('#Modalform').modal();
                    break;
                case 'proyectos':
                    $("#contenedor-principal").load("secciones/proyectoform2.html", function () {
                        $("#cabecera-proyecto > .box-body").load("secciones/proyectoform.html", function () {
                            $("#pro-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-update-pro" class="btn pull-right">Guardar</button> </div>');
                            $("#id_pro").val(values.id_proyecto); 
                            $("#name_pro").val(values.nombre);
                            $("#fecha_inicio").val(values.fecha_inicio);
                            $("#fecha_fin").val(values.fecha_fin);
                            $("#descrip_pro").val(values.descripcion);
                            $('#btn-update-pro').click(function () {
                                var params = {};
                                params.id_proyecto = $("#id_pro").val();
                                params.nombre = $("#name_pro").val();
                                params.anho = values.anho;
                                params.fecha_inicio = $("#fecha_inicio").val();
                                params.fecha_fin = $("#fecha_fin").val();
                                params.modificado = values.modificado;
                                params.descripcion = $("#descrip_pro").val();
                                $.ajax({
                                    url: "https://localhost:44381/api/proyectos",
                                    type: "PUT",
                                    data: JSON.stringify(params),
                                    contentType: 'application/json',
                                    success: function (response) {
                                        console.log(response);

                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus, errorThrown);
                                    }


                                });
                                $('.close').trigger("click");
                            });
                        });
                        grilla_procesos.init('#grilla-user-history', 'user_history', values.id_proyecto);
                        $('#grilla-user-history .box-tools button').click(function (e) {
                            if ($(this).find('i')[0].className === 'fa fa-minus') {
                                $('#grilla-user-history').css('height', 'auto');
                            } else {
                                grilla_procesos.ajustar_panel();
                            }
                        });
                    });
                    break;
                case 'sprints':
                    console.log(values);
                    $("#contenedor-principal").load("secciones/sprintform2.html", function () {
                        $("#cabecera-sprint > .box-body").load("secciones/sprintform.html", function () {
                            $("#sprint-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-update-spr" class="btn pull-right">Guardar</button> </div>');
                            $("#id_spr").val(values.id_spr);
                            $("#name_sprint").val(values.titulo);
                            $("#fecha_inicio_spr").val(values.fecha_inicio);
                            $("#fecha_fin_spr").val(values.fecha_fin);
                            $("#descrip_spr").val(values.descripcion);
                            $('#btn-update-spr').click(function () {
                                var params = {};
                                params.id_proyecto = $("#id_pro").val();
                                params.nombre = $("#name_pro").val();
                                params.anho = values.anho;
                                params.fecha_inicio = $("#fecha_inicio").val();
                                params.fecha_fin = $("#fecha_fin").val();
                                params.modificado = values.modificado;
                                params.descripcion = $("#descrip_pro").val();
                                $.ajax({
                                    url: "https://localhost:44381/api/proyectos",
                                    type: "PUT",
                                    data: JSON.stringify(params),
                                    contentType: 'application/json',
                                    success: function (response) {
                                        console.log(response);

                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus, errorThrown);
                                    }


                                });
                                $('.close').trigger("click");
                            });
                        });
                        grilla_procesos.init('#grilla-sprint-backlog', 'sprint_backlog', values.id_spr);
                        $('#grilla-sprint-backlog .box-tools button').click(function (e) {
                            if ($(this).find('i')[0].className === 'fa fa-minus') {
                                $('#grilla-sprint-backlog').css('height', 'auto');
                            } else {
                                grilla_procesos.ajustar_panel();
                            }
                        });
                    });
                    break;
                case 'user_history':
                    $(".modal-body").css("height", "355px");
                    $(".modal-title").html('Modificar User History');
                    $("#Modalform .modal-body").load("secciones/userhistoryform.html", function () {
                        //console.log(values.id);
                        $("#us-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-update-us" class="btn pull-right">Guardar</button> </div>');
                        $("#id_us").val(values.id_us);
                        $("#titulo_us").val(values.titulo);
                        $("#tiempo_estimado").val(values.tiempo_estimado); 
                        $("#descrip_us").val(values.descripcion);
                        $('#btn-update-us').click(function () {
                            var params = {};
                            params.id_us = $("#id_us").val();
                            params.titulo = $("#titulo_us").val();
                            params.tiempo_estimado = $("#tiempo_estimado").val();
                            params.descripcion = $("#descrip_us").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/userhistorys",
                                type: "PUT",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    grilla_procesos.init('#grilla-user-history', 'user_history', $("#id_pro").val());
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                            $('.close').trigger("click");
                        });
                    });
                    $('#Modalform').modal();
                    break;
                case 'sprint_backlog':
                    $(".modal-body").css("height", "355px");
                    $(".modal-title").html('Modificar Sprint Backlog');
                    $("#Modalform .modal-body").load("secciones/sprintbacklogform.html", function () {
                        //console.log(values.id);
                        $("#us-form").append('<div class="form-group col-sm-12"><button type="button" id="btn-update-us" class="btn pull-right">Guardar</button> </div>');
                        $("#id_us").val(values.id_us);
                        $("#titulo_us").val(values.titulo);
                        $("#tiempo_estimado").val(values.tiempo_estimado);
                        $("#descrip_us").val(values.descripcion);
                        $('#btn-update-us').click(function () {
                            var params = {};
                            params.id_us = $("#id_us").val();
                            params.titulo = $("#titulo_us").val();
                            params.tiempo_estimado = $("#tiempo_estimado").val();
                            params.descripcion = $("#descrip_us").val();
                            console.log(JSON.stringify(params));
                            $.ajax({
                                url: "https://localhost:44381/api/userhistorys",
                                type: "PUT",
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (response) {
                                    console.log(response);
                                    grilla_procesos.init('#grilla-user-history', 'user_history', $("#id_pro").val());
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }


                            });
                            $('.close').trigger("click");
                        });
                    });
                    $('#Modalform').modal();
                    break;
            }
            
           
        }
       // $('#Modalform').modal();

    }
};

function arrancarSelectsAsignado() {
   /* $('#cliente_id').select2({
        tokenSeparators: [','],
        placeholder: 'Buscar cliente',
        ajax: {
            dataType: 'json',
            url: url + '/clienteproceso',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, param) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id,
                            email: item.email,
                            mostrar: resaltar_busqueda(item.text, param.term),
                        }
                    })
                };
            },
        },

        templateResult: function template(data) {
            return (data.id) ? data.mostrar : data.text;
        },
        escapeMarkup: function (m) {
            return m;
        }

    });
    $('#cliente_id').on('select2:select', function (e) {
        var data = e.params.data;
        $("#cliente_email").val(data.email);
    });*/

    $('#us_sb').select2({
        tokenSeparators: [','],
        placeholder: 'Buscar usuario',
        ajax: {
            dataType: 'json',
            url: 'https://localhost:44381/api/users',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term
                }
            },

            processResults: function (data, param) {
                return {
                    results: data.items
                };
            },

        }

        /*templateResult: function template(data) {
            return (data.id) ? data.mostrar : data.text;
        },
        escapeMarkup: function (m) {
            return m;
        }*/
    });

}