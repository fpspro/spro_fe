## PRE REQUISITOS
-PostgreSQL
-pgAdmin

## PASOS
## 1. Para mayor facilidad usar Visual Studio Comunity, 
Descarga de la página oficial de microsoft la version gratuita de visual studio comunity https://visualstudio.microsoft.com/es/downloads/
## 2. Con Git Bash o Team Explorer propio del Visual Studio clona el repositorio
## 3. Ya descargado el repositorio clic en el archivo .sln
## 4. Inicia el pgAdmin 
## 5. Abre el archivo appsenttings.json, ajusta tu user, password, y dbname en "DefaultConnection"
## 6. Clic en el botón verde para el debug
## 7. Al abrise el navegador por primera vez te mostrará un botón para ejecutar la migración de la DB, clic en el botón
## 8. Resfresca la página o apaga el debug y vuelve a iniciarlo